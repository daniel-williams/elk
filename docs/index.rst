Elk - A waveform catalogue tool
===============================

`Elk` is a python package designed to make working with numerical relativity waveforms easier in gravitational wave analyses.

Often numerical relativity data is distributed simply as files, or stored in a directory of files.
`Elk` provides a means of accessing these easily using Python, and make handling these data, and performing processing and reduction on them easier.

Documentation
=============

.. toctree::
   :maxdepth: 2

   readme
   installation
   usage

Tutorials
=========

.. toctree::
   projection

Users' Guide
============

.. toctree::
   waveforms

Developers' Guide
=================
.. toctree::
   
   contributing
   authors
   history

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
